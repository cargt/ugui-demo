/*
 * task_webserver.h
 *
 *  Created on: Jan 8, 2018
 *      Author: mark
 */

#ifndef WEBSERVER_TASK_WEBSERVER_H_
#define WEBSERVER_TASK_WEBSERVER_H_

int task_webserver_init(void);
void task_webserver_event_handler(void *ctx, system_event_t *event);

#endif /* WEBSERVER_TASK_WEBSERVER_H_ */
