/*
 * wifi_scan.c
 *
 *  Created on: Oct 31, 2017
 *      Author: cargt
 */

#include <string.h>

#include "esp_err.h"
#include "esp_wifi.h"
#include "esp_wifi_types.h"

#include "wifi_scan.h"


//=============================================================================
// Private variable
//=============================================================================

static bool scanAsync = false;
static bool scanStarted = false;
static bool scanComplete = false;
static bool scanValid = false;  // could be combined with scanCount of 0 to mean invalid.
static uint16_t scanCount = 0;

#define MAX_WIFI_AP_RECORDS 12
static wifi_ap_record_t apScanResult[MAX_WIFI_AP_RECORDS];


//=============================================================================
// Private prototypes
//=============================================================================
static void scanDelete(void);

//=============================================================================
// Public functions
//=============================================================================

wifi_ap_record_t* WS_getNetworkInfo(uint8_t networkItem)
{
	if((0 == scanCount) || (networkItem >= scanCount))
		return NULL; // error

	return &apScanResult[networkItem];
} // WS_getNetworkInfo

int8_t WS_isScanComplete(void)
{
	printf("WS_isScanComplete %d %d %d",scanStarted,scanComplete,scanCount);
	if(scanStarted)  return WIFI_SCAN_RUNNING;
	if(scanComplete) return scanCount;
	return WIFI_SCAN_FAILED;

} // WS_isScanComplete

void WS_rxScanCompletionEventCallback(system_event_sta_scan_done_t *scanDoneEvent)
{
	// Called by event handler - be concise
	scanComplete = true;
	scanStarted = false;
	esp_wifi_scan_get_ap_num(&scanCount);
	printf("WIFI-SCAN AP COUNT %d\n",scanCount);
	if(scanCount > 0)
	{
		// Limit ssid list to max allowed by memory
		scanCount = (scanCount <= MAX_WIFI_AP_RECORDS) ? scanCount : MAX_WIFI_AP_RECORDS;
		printf("WIFI-SCAN AP COUNT %d\n",scanCount);
		esp_err_t ret = esp_wifi_scan_get_ap_records(&scanCount, (wifi_ap_record_t *)&apScanResult);
		printf("WIFI-SCAN AP COUNT %d\n",scanCount);
		ESP_ERROR_CHECK(ret);
		if(ESP_OK != ret)
		{
			printf("WIFI-SCAN ret error T %d",ret);
			scanValid = false;
			scanCount = 0;
		}
		else
			scanValid = true;
	}

	printf("WIFI-SCAN AP COUNT %d\n",scanCount);
} // WS_rxScanCompletionEventCallback

wifi_scan_status_t WS_scanNetworks(bool async, bool showHidden, bool passive, uint32_t maxMsPerChan)
{
	if(scanStarted)
	{
		return WIFI_SCAN_RUNNING;
	}

	scanAsync = async;
	scanDelete();

	wifi_scan_config_t scanCfg = {
		.ssid =  NULL,
		.bssid = NULL,
		.channel = 0,
		.show_hidden = showHidden,
	};
	if(passive)
	{
		scanCfg.scan_type = WIFI_SCAN_TYPE_PASSIVE;
		scanCfg.scan_time.passive = maxMsPerChan;
	}
	else
	{
		scanCfg.scan_type = WIFI_SCAN_TYPE_ACTIVE;
		scanCfg.scan_time.active.min = 50;
		scanCfg.scan_time.active.max = maxMsPerChan;
	}

	esp_err_t ret = esp_wifi_scan_start(&scanCfg, false);
    ESP_ERROR_CHECK(ret);
    if(ESP_OK == ret)
    {
    	scanComplete = false;
    	scanStarted = true;
        if(async)
        	return WIFI_SCAN_RUNNING;

        while(!(scanComplete))
        {
        	vTaskDelay(pdMS_TO_TICKS(1000));
        }
        return scanCount;
    }
    else
    {
    	return WIFI_SCAN_FAILED;
    }

} // WS_scanNetworks

//=============================================================================
// Private functions
//=============================================================================

static void scanDelete(void)
{
	scanValid = false;
	memset(&apScanResult, 0, sizeof apScanResult);
} // scanDelete

