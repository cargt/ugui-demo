
#include "sdkconfig.h"
#include "string.h"
#include "ugui.h"
#include "ugui_msg.h"
#include "ugui_helper.h"
#include "ugui_keyboard.h"
#include "ugui_settings_network_wifi.h"

#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_event_loop.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "task_network.h"

#include "wifi_scan.h"

static const char *TAG = "wifi_window";

//ssid
#define SSID_HIDDEN_STR "< hidden SSID >"
#define SSID_MAX_LENGTH 32
#define MAX_STR 256
static char ssid_str[SSID_MAX_LENGTH];
static char password_str[MAX_STR];

#define MAX_SCROLL_OBJECTS      12
#define SCROLL_DOTS_CNT			DOT_GET_NUM_OF_DOTS(MAX_SCROLL_OBJECTS)

#define TXB_ID_HEADER				TXB_ID_0
#define IMG_ID_LOGO					IMG_ID_1
#define IMG_ID_BATTERY				IMG_ID_2
#define IMG_ID_WIFI					IMG_ID_3
#define TXB_ID_SETTINGS				TXB_ID_4
#define BUTTON_ID_BACK 				BTN_ID_5
#define BUTTON_ID_SCROLL_UP			BTN_ID_6
#define BUTTON_ID_SCROLL_DOWN		BTN_ID_7
#define IMG_ID_SCROLL_DOT_FIRST		IMG_ID_8
#define IMG_ID_SCROLL_DOT_LAST		IMG_ID_SCROLL_DOT_FIRST + SCROLL_DOTS_CNT
#define BUTTON_ID_SCROLL_FIRST		IMG_ID_SCROLL_DOT_LAST
#define BUTTON_ID_SCROLL_LAST		BUTTON_ID_SCROLL_FIRST + MAX_SCROLL_OBJECTS
#define MAX_OBJECTS        			BUTTON_ID_SCROLL_LAST

#define BUTTON_ID_ACCOUNT	BUTTON_ID_SCROLL_FIRST
#define BUTTON_ID_WIFI		(BUTTON_ID_SCROLL_FIRST+1)
#define BUTTON_ID_ABOUT		(BUTTON_ID_SCROLL_FIRST+2)

/* Window 1 */
static UG_WINDOW window_1;
static UG_OBJECT obj_buff_wnd_1[MAX_OBJECTS];
static UG_TEXTBOX tb_header;

//buttons
static UG_TEXTBOX tb_settings;
static UG_IMAGE img_battery;
static UG_IMAGE img_wifi;
static UG_BUTTON button_back;
static UG_BUTTON button_scroll_up;
static UG_BUTTON button_scroll_down;
static UG_IMAGE img_scroll_dot[SCROLL_DOTS_CNT];
static UG_BUTTON button_scroll_items[MAX_SCROLL_OBJECTS];
static UG_U16 scroll_offset;
static UG_U16 scroll_offset_current;
static UG_S16 scroll_offset_max;
static UG_S8 scroll_objects;
static UG_AREA scroll_area;
static UG_AREA button_scroll_items_area[MAX_SCROLL_OBJECTS];

static char scroll_item_txt[MAX_SCROLL_OBJECTS][SSID_MAX_LENGTH];

static void touch_callback( UG_MESSAGE* msg );
static void scroll_update(UG_U8 force);
static void window_update( ugui_system_event_t evt );
static void my_keyboard_callback();
static void my_keyboard_ssid_callback();
static uint8_t is_network_open(char* ssid);

UG_RESULT UG_SettingsNetworkWifiSsidCreate()
{
	/*******************************************************
	 * Window 1
	 *******************************************************/
	UG_WindowCreate( &window_1, obj_buff_wnd_1, MAX_OBJECTS, touch_callback );
	UG_WindowSetForeColor( &window_1,SETTINGS_WINDOW_FORECOLOR );
	UG_WindowSetBackColor( &window_1,SETTINGS_WINDOW_BACKCOLOR );
	UG_WindowSetStyle( &window_1, WINDOW_STYLE);

	UG_S16 width = UG_WindowGetInnerWidth(&window_1);
	UG_S16 height = UG_WindowGetInnerHeight(&window_1);

	/*******************************************************
	 * Window Header
	 ******************************************************/

	// text box - window header
	UG_TextboxCreate( &window_1, &tb_header,TXB_ID_HEADER,0,0,width,WINDOW_HEADER_H);
	UG_TextboxSetFont( &window_1, TXB_ID_HEADER, &TXB_HEADER_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_HEADER, TXB_HEADER_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_HEADER, TXB_HEADER_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_HEADER, ALIGN_CENTER );
	UG_TextboxSetText( &window_1, TXB_ID_HEADER, "Cargt uGUI" );

	// image - battery
	UG_ImageCreate( &window_1, &img_battery, IMG_ID_BATTERY,
			HEADER_BATTERY_XS,
			HEADER_BATTERY_YS,
			HEADER_BATTERY_XE,
			HEADER_BATTERY_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_BATTERY, &HEADER_BATTERY_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_BATTERY, 1, TXB_HEADER_BACKCOLOR );

	// image - wifi
	UG_ImageCreate( &window_1, &img_wifi, IMG_ID_WIFI,
			HEADER_WIFI_XS,
			HEADER_WIFI_YS,
			HEADER_WIFI_XE,
			HEADER_WIFI_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_WIFI, &HEADER_WIFI_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_WIFI, 1, TXB_HEADER_BACKCOLOR );

	/*******************************************************
	 * Window Specific items
	 ******************************************************/
	// text box
	UG_TextboxCreate( &window_1, &tb_settings,TXB_ID_SETTINGS,SETTINGS_TXB_XS,SETTINGS_TXB_YS,SETTINGS_TXB_XE,SETTINGS_TXB_YE-1);
	UG_TextboxSetFont( &window_1, TXB_ID_SETTINGS, &SETTINGS_TXB_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_SETTINGS, SETTINGS_TXB_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_SETTINGS, SETTINGS_TXB_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_SETTINGS, SETTINGS_TXB_ALIGNMENT );
	UG_TextboxSetText( &window_1, TXB_ID_SETTINGS, "Scanning..." );

	//back
	UG_ButtonCreate( &window_1, &button_back, BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_XS,SETTINGS_BUTTON_BACK_YS,SETTINGS_BUTTON_BACK_XE,SETTINGS_BUTTON_BACK_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_BACK, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_STYLE);
	UG_ButtonSetImage( &window_1, BUTTON_ID_BACK, BTN_IMG_CENTER, &SETTINGS_BUTTON_BACK_ICON, SETTINGS_BUTTON_BACK_ICON.width, SETTINGS_BUTTON_BACK_ICON.height );

	//Scroll items
	scroll_area.xs = SETTINGS_SCROLL_AREA_XS;
	scroll_area.xe = SETTINGS_SCROLL_AREA_XE;
	scroll_area.ys = SETTINGS_SCROLL_AREA_YS;
	scroll_area.ye = SETTINGS_SCROLL_AREA_YE;

	for(int i=0; i<MAX_SCROLL_OBJECTS; i++)
	{
		button_scroll_items_area[i].xs = scroll_area.xs;
		button_scroll_items_area[i].xe = scroll_area.xe;
		button_scroll_items_area[i].ys = scroll_area.ys + SETTINGS_BUTTON_H * i;
		button_scroll_items_area[i].ye = button_scroll_items_area[i].ys + SETTINGS_BUTTON_H;

		UG_ButtonCreate( &window_1, &button_scroll_items[i], BUTTON_ID_SCROLL_FIRST+i, button_scroll_items_area[i].xs,button_scroll_items_area[i].ys,button_scroll_items_area[i].xe,button_scroll_items_area[i].ye);
		UG_ButtonSetFont( &window_1, 		BUTTON_ID_SCROLL_FIRST+i, &FONT_12 );
		UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SCROLL_FIRST+i, SETTINGS_BUTTON_FORECOLOR );
		UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SCROLL_FIRST+i, SETTINGS_BUTTON_BACKCOLOR );
		UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SCROLL_FIRST+i, SETTINGS_BUTTON_STYLE);
		UG_ButtonSetText( &window_1, 		BUTTON_ID_SCROLL_FIRST+i, scroll_item_txt[i] );
		UG_ButtonSetAlignment( &window_1, 	BUTTON_ID_SCROLL_FIRST+i, ALIGN_CENTER_LEFT );
		UG_ButtonSetImage( &window_1, 		BUTTON_ID_SCROLL_FIRST+i, BTN_IMG_LEFT, NULL, 1, 0 );

		UG_ButtonSetAreaFrame( &window_1,   BUTTON_ID_SCROLL_FIRST+i, scroll_area.xs, scroll_area.ys, scroll_area.xe, scroll_area.ye);
		UG_ButtonHide( &window_1,			BUTTON_ID_SCROLL_FIRST+i);
	}

	scroll_offset = 0;
	scroll_offset_current = 0;
	scroll_offset_max = 0;
	scroll_objects = 0;
	scroll_offset_max = MAX_SCROLL_OBJECTS * SETTINGS_BUTTON_H - (SETTINGS_SCROLL_AREA_YE - SETTINGS_SCROLL_AREA_YS);

	//BUTTON_ID_SCROLL_UP
	UG_ButtonCreate( &window_1, &button_scroll_up, BUTTON_ID_SCROLL_UP, SETTINGS_BUTTON_UP_XS,SETTINGS_BUTTON_UP_YS,SETTINGS_BUTTON_UP_XE,SETTINGS_BUTTON_UP_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_SCROLL_UP, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SCROLL_UP, SETTINGS_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SCROLL_UP, SETTINGS_BUTTON_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SCROLL_UP, ( BTN_STYLE_TOGGLE_COLORS | BTN_STYLE_NO_BORDERS ));
	UG_ButtonSetImage( &window_1, BUTTON_ID_SCROLL_UP, BTN_IMG_CENTER, &SETTINGS_BUTTON_UP_ICON, SETTINGS_BUTTON_UP_ICON.width, SETTINGS_BUTTON_UP_ICON.height );
	UG_ButtonHide( &window_1, BUTTON_ID_SCROLL_UP );

	//BUTTON_ID_SCROLL_DOWN
	UG_ButtonCreate( &window_1, &button_scroll_down, BUTTON_ID_SCROLL_DOWN, SETTINGS_BUTTON_DOWN_XS,SETTINGS_BUTTON_DOWN_YS,SETTINGS_BUTTON_DOWN_XE,SETTINGS_BUTTON_DOWN_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_SCROLL_DOWN, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SCROLL_DOWN, SETTINGS_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SCROLL_DOWN, SETTINGS_BUTTON_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SCROLL_DOWN, ( BTN_STYLE_TOGGLE_COLORS | BTN_STYLE_NO_BORDERS ));
	UG_ButtonSetImage( &window_1, BUTTON_ID_SCROLL_DOWN, BTN_IMG_CENTER, &SETTINGS_BUTTON_DOWN_ICON, SETTINGS_BUTTON_DOWN_ICON.width, SETTINGS_BUTTON_DOWN_ICON.height );
	UG_ButtonHide( &window_1, BUTTON_ID_SCROLL_DOWN );

	for(int i=0; i<SCROLL_DOTS_CNT; i++)
	{
		// image - scroll dot 1
		UG_ImageCreate( &window_1, &img_scroll_dot[i], IMG_ID_SCROLL_DOT_FIRST+i,
				SETTINGS_SCROLL_DOTS_AREA_XS,
				DOT_GET_YS(SCROLL_DOTS_CNT,i),
				SETTINGS_SCROLL_DOTS_AREA_XE,
				DOT_GET_YE(SCROLL_DOTS_CNT,i) );
		UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, 1, SETTINGS_WINDOW_BACKCOLOR );
		if(i==0)
		{
			UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_FULL_ICON );
		}
		else
		{
			UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_EMPTY_ICON );
		}
		UG_ImageHide(&window_1, IMG_ID_SCROLL_DOT_FIRST+i);
	}

	return UG_RESULT_OK;

}

UG_RESULT UG_SettingsNetworkWifiSsidShow()
{
	task_network_sendStartScanEvent();
	return UG_WindowShow( &window_1, window_update );
}

UG_RESULT UG_SettingsNetworkWifiSdidHide()
{
	return UG_WindowHide( &window_1 );
}

void UG_SettingsNetworkWifiSsidUpdate()
{
    int8_t numberOfAP = WS_isScanComplete();
    wifi_ap_record_t* apData;
    int i;

    UG_TextboxSetText( &window_1, TXB_ID_SETTINGS, "Choose Network" );

    ESP_LOGI(TAG, "numberOfAP %d",numberOfAP);
    if(numberOfAP > MAX_SCROLL_OBJECTS)
    {
    	numberOfAP = MAX_SCROLL_OBJECTS;
    }

    ESP_LOGI(TAG, "numberOfAP %d",numberOfAP);
    for(i=0; i<numberOfAP; i++)
    {
    	apData = WS_getNetworkInfo(i);
        if(NULL != apData && strcmp(scroll_item_txt[i],(char*)apData->ssid) != 0)
        {
        	ESP_LOGI(TAG, "AP: %S",(wchar_t *)apData->ssid);
        	strcpy(scroll_item_txt[i],(char*)apData->ssid);
        	UG_ButtonSetText( &window_1, BUTTON_ID_SCROLL_FIRST+i, (const char*)scroll_item_txt[i]);
        	UG_ButtonShow(&window_1,BUTTON_ID_SCROLL_FIRST+i);

        	if(strlen(scroll_item_txt[i])>20)
        	{
        		UG_ButtonSetFont( &window_1, 		BUTTON_ID_SCROLL_FIRST+i, &FONT_8 );
        	}
        }
    }
    //add item for hidden ssid
    if(i<MAX_SCROLL_OBJECTS)
    {
    	UG_ButtonSetText( &window_1, BUTTON_ID_SCROLL_FIRST+i, SSID_HIDDEN_STR);
    	UG_ButtonHide(&window_1,BUTTON_ID_SCROLL_FIRST+i);
    	i++;
    }

    // clear the reset of the list
    for(/*i=i*/; i<MAX_SCROLL_OBJECTS; i++)
    {
    	UG_ButtonSetText( &window_1, BUTTON_ID_SCROLL_FIRST+i, " ");
    	UG_ButtonHide(&window_1,BUTTON_ID_SCROLL_FIRST+i);
    }

    scroll_objects = numberOfAP;
    scroll_update(1);

}

static void window_update( ugui_system_event_t evt )
{
	switch(evt)
	{
	case UGUI_SYSTEM_EVENT_TICK:
		scroll_update(0);
		ugui_icon_wifi_update(&window_1,IMG_ID_WIFI);
		ugui_icon_battery_update(&window_1,IMG_ID_BATTERY);
		break;
	case UGUI_SYSTEM_EVENT_WIFI_SCAN_DONE:
		ESP_LOGI(TAG,"UGUI_SYSTEM_EVENT_WIFI_SCAN_DONE");
		UG_SettingsNetworkWifiSsidUpdate();
		break;
	default:
		break;
	}
}

static void scroll_update(UG_U8 force)
{
	//show/hide scroll
	if(force)
	{
		scroll_offset_current = 0;
		int i;
		int num_dots = DOT_GET_NUM_OF_DOTS(scroll_objects);
		for(i=0; i<num_dots; i++)
		{
			UG_ImageMove(&window_1, IMG_ID_SCROLL_DOT_FIRST+i,
					SETTINGS_SCROLL_DOTS_AREA_XS,
					DOT_GET_YS(num_dots,i),
					SETTINGS_SCROLL_DOTS_AREA_XE,
					DOT_GET_YE(num_dots,i) );
			UG_ImageShow(&window_1, IMG_ID_SCROLL_DOT_FIRST+i);
		}
		for(/*i=i*/; i<SCROLL_DOTS_CNT; i++)
		{
			UG_ImageHide(&window_1, IMG_ID_SCROLL_DOT_FIRST+i);
		}

		if(num_dots > 0)
		{
			UG_ButtonShow( &window_1, BUTTON_ID_SCROLL_UP );
			UG_ButtonShow( &window_1, BUTTON_ID_SCROLL_DOWN );
		}
	}

	//update scroll
	if(scroll_offset_current != scroll_offset || force > 0)
	{
		if(scroll_offset_current - scroll_offset > SETTINGS_SCROLL_STEP_SIZE)
		{
			scroll_offset_current-=SETTINGS_SCROLL_STEP_SIZE;
		}
		else if(scroll_offset_current > scroll_offset )
		{
			scroll_offset_current = scroll_offset;
		}
		else if(scroll_offset - scroll_offset_current > SETTINGS_SCROLL_STEP_SIZE)
		{
			scroll_offset_current+=SETTINGS_SCROLL_STEP_SIZE;
		}
		else
		{
			scroll_offset_current = scroll_offset;
		}

		for(int i=0; i<MAX_SCROLL_OBJECTS; i++)
		{
			button_scroll_items_area[i].xs = scroll_area.xs;
			button_scroll_items_area[i].xe = scroll_area.xe;
			button_scroll_items_area[i].ys = scroll_area.ys + SETTINGS_BUTTON_H * i - scroll_offset_current;
			button_scroll_items_area[i].ye = button_scroll_items_area[i].ys + SETTINGS_BUTTON_H;
			UG_ButtonMove( &window_1, BUTTON_ID_SCROLL_FIRST+i, button_scroll_items_area[i].xs, button_scroll_items_area[i].ys, button_scroll_items_area[i].xe, button_scroll_items_area[i].ye);

			if(( button_scroll_items_area[i].ys < scroll_area.ye && button_scroll_items_area[i].ys > scroll_area.ys )
					|| ( button_scroll_items_area[i].ye > scroll_area.ys && button_scroll_items_area[i].ye < scroll_area.ye) )
			{
				UG_ButtonShow( &window_1, BUTTON_ID_SCROLL_FIRST+i);
			}
			else
			{
				UG_ButtonHide( &window_1, BUTTON_ID_SCROLL_FIRST+i);
			}
		}

		//update dots
		for(int i=0; i<SCROLL_DOTS_CNT; i++)
		{
			if(scroll_offset == scroll_offset_max )
			{
				if( i == (SCROLL_DOTS_CNT-1) )
				{
					UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_FULL_ICON );
				}
				else
				{
					UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_EMPTY_ICON );
				}
			}
			else if(scroll_offset >= SETTINGS_SCROLL_PAGE_SIZE*i && scroll_offset < SETTINGS_SCROLL_PAGE_SIZE*(i+1) )
			{
				UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_FULL_ICON );
			}
			else
			{
				UG_ImageSetBMP( &window_1, IMG_ID_SCROLL_DOT_FIRST+i, &SETTINGS_SCROLL_DOTS_EMPTY_ICON );
			}
		}
	}
}

static void touch_callback( UG_MESSAGE* msg )
{
	if ( msg->type == MSG_TYPE_OBJECT )
	{
		if ( msg->id == OBJ_TYPE_BUTTON && msg->event == OBJ_EVENT_RELEASED )
		{
			switch(msg->sub_id)
			{
			case BUTTON_ID_SCROLL_UP:
				if( scroll_offset < SETTINGS_SCROLL_PAGE_SIZE )
				{
					scroll_offset = 0;
				}
				else
				{
					scroll_offset -= SETTINGS_SCROLL_PAGE_SIZE;
				}
				break;
			case BUTTON_ID_SCROLL_DOWN:
				scroll_offset += SETTINGS_SCROLL_PAGE_SIZE;
				if( scroll_offset > scroll_offset_max )
				{
					scroll_offset = scroll_offset_max;
				}
				break;
			case BUTTON_ID_BACK:
				UG_SettingsNetworkWifiShow();
				break;
			}

			if( msg->sub_id >= BUTTON_ID_SCROLL_FIRST && msg->sub_id <= BUTTON_ID_SCROLL_LAST )
			{
				//hidden ssid
				if( strcmp(SSID_HIDDEN_STR,UG_ButtonGetText( &window_1, msg->sub_id) ) == 0 )
				{
					memset(ssid_str,0,SSID_MAX_LENGTH);
					UG_KeyboardShow(UGUI_KEYBOARD_KEYS_STD,"Enter Wi-Fi SSID",ssid_str, SSID_MAX_LENGTH, my_keyboard_ssid_callback);
				}
				//selected ssid
				else
				{
					strcpy(ssid_str, UG_ButtonGetText( &window_1, msg->sub_id));
					memset(password_str,0,MAX_STR);
					if(is_network_open(ssid_str))
					{
						my_keyboard_callback();
					}
					else
					{
						UG_KeyboardShow(UGUI_KEYBOARD_KEYS_STD,"Enter Wi-Fi password",password_str, MAX_STR, my_keyboard_callback);
					}
				}
			}

		}
	}

}

static void my_keyboard_ssid_connecting_callback()
{
	//cancel button
	UG_SettingsNetworkWifiSsidShow();
}

static void my_keyboard_callback()
{
	//connect to wifi
	task_network_updateSSIDAndPassword(ssid_str, password_str);
	//push wifi status page
	UG_MsgShow("Connecting",
			0,
			UGUI_MSG_ICON_WORKING,
			UGUI_MSG_STYLE_1_BUTTON,
			"Cancel",
			0,
			my_keyboard_ssid_connecting_callback,
			0,
			UGUI_SYSTEM_EVENT_WIFI_CONNECT_SUCCESS | UGUI_SYSTEM_EVENT_WIFI_CONNECT_FAIL);

}

static void my_keyboard_ssid_callback()
{
	memset(password_str,0,MAX_STR);
	UG_KeyboardShow(UGUI_KEYBOARD_KEYS_STD,"Enter Wi-Fi password",password_str, MAX_STR, my_keyboard_callback);
}

static uint8_t is_network_open(char* ssid)
{
    int8_t numberOfAP = WS_isScanComplete();
    wifi_ap_record_t* apData;
    int i;

    ESP_LOGI(TAG, "is_network_open AP NUM: %d ",numberOfAP);
    for(i=0; i<numberOfAP; i++)
    {
    	apData = WS_getNetworkInfo(i);
        if(NULL != apData)
        {
        	ESP_LOGI(TAG, "AP: %S %s ",(wchar_t *)apData->ssid, ssid);
        	if(strcmp((char*)apData->ssid, ssid) == 0)
        	{
        		if(apData->authmode == WIFI_AUTH_OPEN)
        		{
        			ESP_LOGI(TAG, "AP: FOUND");
        			return 1;
        		}
        	}
        }
    }

    return 0;
}

