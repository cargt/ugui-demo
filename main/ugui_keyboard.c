#include <string.h>
#include "ugui_keyboard.h"
#include "ugui_config.h"


//qwerty
#define KB_ROW_1_QWERTY 10
#define KB_ROW_2_QWERTY 9
#define KB_ROW_3_QWERTY 7
#define KB_TOTAL_QWERTY (KB_ROW_1_QWERTY+KB_ROW_2_QWERTY+KB_ROW_3_QWERTY)

static const char* keys_qwerty = 0;
static const char* keys_qwerty_upper = 0;
static const char* keys_numbers = 0;
static const char* keys_numbers_2 = 0;
static const char *kb_ptr;

extern const char keys_qwerty_std[];
extern const char keys_qwerty_upper_std[];
extern const char keys_numbers_std[];
extern const char keys_numbers_2_std[];
extern const char keys_numbers_email[];

//buttons
#define BUTTON_ID_SHIFT 			KB_TOTAL_QWERTY
#define BUTTON_ID_BACKSPACE 		(BUTTON_ID_SHIFT + 1)
#define BUTTON_ID_SPACE 			(BUTTON_ID_BACKSPACE + 1)
#define BUTTON_ID_KEYBOARD_CHANGE	(BUTTON_ID_SPACE + 1)
#define BUTTON_ID_ENTER 			(BUTTON_ID_KEYBOARD_CHANGE + 1)
#define BUTTON_ID_BACK				(BUTTON_ID_ENTER + 1)
#define BUTTON_ID_AT				(BUTTON_ID_BACK + 1)
#define BUTTON_ID_PERIOD			(BUTTON_ID_AT + 1)
#define BUTTON_ID_DOTCOM			(BUTTON_ID_PERIOD + 1)

#define TOTAL_KEYS 	(BUTTON_ID_DOTCOM + 1)
static UG_BUTTON* button_keys;//[TOTAL_KEYS];
static UG_BUTTON button_back;
static ugui_keyboard_keys_t keyboard = UGUI_KEYBOARD_KEYS_STD;

#define MAX_OBJECTS        (2 + TOTAL_KEYS)

//used for callback
keyboard_callback kb_callback;
static char* kb_string = 0;
static UG_U32 kb_stringlen = 0;

/* Window 1 */
static UG_WINDOW window_1;
static UG_OBJECT* obj_buff_wnd_1;//[MAX_OBJECTS];
static UG_TEXTBOX tb;

static void keyboard_scroll();
static void keyboard_change(UG_U8 type);
static void window_kb_callback( UG_MESSAGE* msg );

UG_RESULT UG_KeyboardCreate()// UG_WINDOW* wnd, UG_KEYBOARD* kb )
{
	/*******************************************************
	 * allocate memory
	 *******************************************************/
	obj_buff_wnd_1 = heap_caps_malloc(sizeof(UG_OBJECT) * MAX_OBJECTS,MALLOC_CAP_SPIRAM);
	assert(obj_buff_wnd_1);
	button_keys = heap_caps_malloc(sizeof(UG_BUTTON) * TOTAL_KEYS,MALLOC_CAP_SPIRAM);
	assert(button_keys);

	/* Create Window 1 */
	UG_WindowCreate( &window_1, obj_buff_wnd_1, MAX_OBJECTS, window_kb_callback );
	UG_WindowSetForeColor( &window_1, KEYBOARD_WINDOW_FORECOLOR );
	UG_WindowSetBackColor( &window_1,KEYBOARD_WINDOW_BACKCOLOR );
	UG_WindowSetStyle( &window_1, KEYBOARD_WINDOW_STYLE);


	UG_S16 width = UG_WindowGetInnerWidth(&window_1);
	UG_S16 key_width = width / KB_ROW_1_QWERTY;
	UG_S16 height = UG_WindowGetInnerHeight(&window_1);
	UG_S16 key_height = height / 5;

	//back
	UG_ButtonCreate( &window_1, &button_back, BUTTON_ID_BACK,
			0,
			0,
			SETTINGS_BUTTON_BACK_XE,
			key_height-1);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_BACK, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_STYLE);
	UG_ButtonSetImage( &window_1, BUTTON_ID_BACK, BTN_IMG_CENTER, &SETTINGS_BUTTON_BACK_ICON, SETTINGS_BUTTON_BACK_ICON.width, SETTINGS_BUTTON_BACK_ICON.height );

	// text box
	UG_TextboxCreate( &window_1, &tb,TXB_ID_0,
			SETTINGS_BUTTON_BACK_XE,
			0,
			width-1-50,
			key_height-1);
	UG_TextboxSetFont( &window_1, TXB_ID_0, &KEYBOARD_TXB_FONT );
	UG_TextboxSetText( &window_1, TXB_ID_0, kb_string );
	UG_TextboxSetForeColor( &window_1, TXB_ID_0, TXB_HEADER_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_0, TXB_HEADER_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_0, KEYBOARD_TXB_ALIGNMENT );

	/* Create Buttons  for each row*/
	for(int i=0;i<KB_ROW_1_QWERTY; i++)
	{
		UG_ButtonCreate( &window_1, &button_keys[i], i, i*key_width, key_height, i*key_width+key_width, key_height*2 );
		UG_ButtonSetFont( &window_1, i, &KEYBOARD_BUTTON_FONT );
		UG_ButtonSetForeColor( &window_1, i, KEYBOARD_BUTTON_FORECOLOR );
		UG_ButtonSetBackColor( &window_1, i, KEYBOARD_BUTTON_BACKCOLOR );
		UG_ButtonSetText( &window_1, i, &keys_qwerty_std[i*2] );
		UG_ButtonSetStyle( &window_1, i, KEYBOARD_BUTTON_STYLE);
	}

	int offset = KB_ROW_1_QWERTY;
	int h_offset = (KB_ROW_1_QWERTY-KB_ROW_2_QWERTY)*key_width / 2;
	for(int i=0;i<(KB_ROW_2_QWERTY); i++)
	{
		UG_ButtonCreate( &window_1, &button_keys[i+offset], i+offset, i*key_width + h_offset, key_height*2, i*key_width+key_width + h_offset, key_height*3 );
		UG_ButtonSetFont( &window_1, i+offset, &KEYBOARD_BUTTON_FONT );
		UG_ButtonSetForeColor( &window_1, i+offset, KEYBOARD_BUTTON_FORECOLOR );
		UG_ButtonSetBackColor( &window_1, i+offset, KEYBOARD_BUTTON_BACKCOLOR );
		UG_ButtonSetText( &window_1, i+offset, &keys_qwerty_std[(i+offset)*2] );
		UG_ButtonSetStyle( &window_1, i+offset, KEYBOARD_BUTTON_STYLE);
	}

	offset = KB_ROW_1_QWERTY + KB_ROW_2_QWERTY;
	h_offset = (KB_ROW_1_QWERTY-KB_ROW_3_QWERTY)*key_width / 2;
	for(int i=0;i<(KB_ROW_3_QWERTY); i++)
	{
		UG_ButtonCreate( &window_1, &button_keys[i+offset], i+offset, i*key_width + h_offset, key_height*3, i*key_width+key_width + h_offset, key_height*4);
		UG_ButtonSetFont( &window_1, i+offset, &KEYBOARD_BUTTON_FONT );
		UG_ButtonSetForeColor( &window_1, i+offset, KEYBOARD_BUTTON_FORECOLOR );
		UG_ButtonSetBackColor( &window_1, i+offset, KEYBOARD_BUTTON_BACKCOLOR );
		UG_ButtonSetText( &window_1, i+offset, &keys_qwerty_std[(i+offset)*2] );
		UG_ButtonSetStyle( &window_1, i+offset, KEYBOARD_BUTTON_STYLE);
	}

	//shift
	UG_ButtonCreate( &window_1, &button_keys[BUTTON_ID_SHIFT], BUTTON_ID_SHIFT, 1, key_height*3, (key_width*3/2), key_height*4);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_SHIFT, &KEYBOARD_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SHIFT, KEYBOARD_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SHIFT, KEYBOARD_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_SHIFT, "Aa" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SHIFT, KEYBOARD_BUTTON_STYLE);

	//backspace
	UG_ButtonCreate( &window_1, &button_keys[BUTTON_ID_BACKSPACE], BUTTON_ID_BACKSPACE,
			width-1 - 50,
			0,
			width-1,
			key_height-1);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_BACKSPACE, &KEYBOARD_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_BACKSPACE, TXB_HEADER_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_BACKSPACE, TXB_HEADER_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_BACKSPACE, "" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_BACKSPACE, KEYBOARD_BUTTON_STYLE);
	UG_ButtonSetImage( &window_1, 		BUTTON_ID_BACKSPACE, BTN_IMG_CENTER, &img_backspace, img_backspace.width, img_backspace.height );

	//space
	UG_ButtonCreate( &window_1, &button_keys[BUTTON_ID_SPACE], BUTTON_ID_SPACE, 2*key_width + h_offset, key_height*4, 6*key_width + h_offset, height-1);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_SPACE, &KEYBOARD_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_SPACE, C_WHITE_SMOKE );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_SPACE, KEYBOARD_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_SPACE, "Space" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_SPACE, KEYBOARD_BUTTON_STYLE);

	//keyboard change
	UG_ButtonCreate( &window_1, &button_keys[BUTTON_ID_KEYBOARD_CHANGE], BUTTON_ID_KEYBOARD_CHANGE, 1, key_height*4, 2*key_width + h_offset, height-1);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_KEYBOARD_CHANGE, &KEYBOARD_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_KEYBOARD_CHANGE, KEYBOARD_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_KEYBOARD_CHANGE, KEYBOARD_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_KEYBOARD_CHANGE, "?123" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_KEYBOARD_CHANGE, KEYBOARD_BUTTON_STYLE);

	//enter
	UG_ButtonCreate( &window_1, &button_keys[BUTTON_ID_ENTER], BUTTON_ID_ENTER, 6*key_width + h_offset, key_height*4, width-1, height-1);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_ENTER, &KEYBOARD_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_ENTER, KEYBOARD_BUTTON_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_ENTER, KEYBOARD_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_ENTER, "" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_ENTER, KEYBOARD_BUTTON_STYLE);
	UG_ButtonSetImage( &window_1, 		BUTTON_ID_ENTER, BTN_IMG_CENTER, &img_arrow_forward, img_arrow_forward.width, img_arrow_forward.height );

	/*
	 * EMAIL BTNS
	 */
	//AT
	UG_ButtonCreate( &window_1, &button_keys[BUTTON_ID_AT], BUTTON_ID_AT, 2*key_width + h_offset, key_height*4, 3*key_width + h_offset, height-1);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_AT, &KEYBOARD_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_AT, C_WHITE_SMOKE );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_AT, KEYBOARD_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_AT, "@" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_AT, KEYBOARD_BUTTON_STYLE);
	//PERIOD
	UG_ButtonCreate( &window_1, &button_keys[BUTTON_ID_PERIOD], BUTTON_ID_PERIOD, 3*key_width + h_offset, key_height*4, 4*key_width + h_offset, height-1);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_PERIOD, &KEYBOARD_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_PERIOD, C_WHITE_SMOKE );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_PERIOD, KEYBOARD_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_PERIOD, "." );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_PERIOD, KEYBOARD_BUTTON_STYLE);
	//.COM
	UG_ButtonCreate( &window_1, &button_keys[BUTTON_ID_DOTCOM], BUTTON_ID_DOTCOM, 4*key_width + h_offset, key_height*4, 6*key_width + h_offset, height-1);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_DOTCOM, &KEYBOARD_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_DOTCOM, C_WHITE_SMOKE );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_DOTCOM, KEYBOARD_BUTTON_BACKCOLOR );
	UG_ButtonSetText( &window_1, 		BUTTON_ID_DOTCOM, ".com" );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_DOTCOM, KEYBOARD_BUTTON_STYLE);

	return UG_RESULT_OK;
}

void UG_KeyboardShow(ugui_keyboard_keys_t keyboard, char* init_string, char* ret_string, UG_U32 stringlen, keyboard_callback callback )
{
	kb_callback = callback;
	kb_string = ret_string;
	kb_stringlen = stringlen-1;

	if(init_string)
	{
		UG_TextboxSetText( &window_1, TXB_ID_0, init_string );
	}
	else
	{
		UG_TextboxSetText( &window_1, TXB_ID_0, ret_string );
	}
	keyboard_scroll();

	switch(keyboard)
	{
	case UGUI_KEYBOARD_KEYS_EMAIL:
		keys_qwerty = keys_qwerty_std;
		keys_qwerty_upper = keys_qwerty_upper_std;
		keys_numbers = keys_numbers_email;
		keys_numbers_2 = keys_numbers_2_std;
		UG_ButtonShow( &window_1, BUTTON_ID_DOTCOM );
		UG_ButtonShow( &window_1, BUTTON_ID_PERIOD );
		UG_ButtonShow( &window_1, BUTTON_ID_AT );
		UG_ButtonHide( &window_1, BUTTON_ID_SPACE );
		break;
	case UGUI_KEYBOARD_KEYS_STD:
	default:
		keys_qwerty = keys_qwerty_std;
		keys_qwerty_upper = keys_qwerty_upper_std;
		keys_numbers = keys_numbers_std;
		keys_numbers_2 = keys_numbers_2_std;
		UG_ButtonHide( &window_1, BUTTON_ID_DOTCOM );
		UG_ButtonHide( &window_1, BUTTON_ID_PERIOD );
		UG_ButtonHide( &window_1, BUTTON_ID_AT );
		UG_ButtonShow( &window_1, BUTTON_ID_SPACE );
		break;
	}

	keyboard_change(KB_TYPE_QWERTY_LOWER);
	UG_WindowShow( &window_1, NULL );
}

void UG_KeyboardHide()
{
	UG_WindowHide( &window_1 );
}

/* Callback function for the main menu */
static void window_kb_callback( UG_MESSAGE* msg )
{
	if ( msg->type == MSG_TYPE_OBJECT )
	{
		if ( msg->id == OBJ_TYPE_BUTTON && msg->event == OBJ_EVENT_RELEASED )
		{
			if( msg->sub_id < KB_TOTAL_QWERTY )
			{
				if(strlen(kb_string)<kb_stringlen)
				{
					kb_string[strlen(kb_string)] = kb_ptr[msg->sub_id*2];
					UG_TextboxSetText( &window_1, TXB_ID_0, kb_string );
				}
			}
			switch(msg->sub_id)
			{
			case BUTTON_ID_SHIFT:
				if(kb_ptr == keys_numbers )
				{
					keyboard_change(KB_TYPE_NUMBERS_2);
				}
				else if(kb_ptr == keys_numbers_2 )
				{
					keyboard_change(KB_TYPE_NUMBERS);
				}
				else if(kb_ptr == keys_qwerty)
				{
					keyboard_change(KB_TYPE_QWERTY_UPPER);
				}
				else
				{
					keyboard_change(KB_TYPE_QWERTY_LOWER);
				}
				break;
			case BUTTON_ID_BACKSPACE:
				if(strlen(kb_string)>0)
				{
					kb_string[strlen(kb_string)-1] = 0;
					UG_TextboxSetText( &window_1, TXB_ID_0, kb_string );
				}
				break;
			case BUTTON_ID_SPACE:
				if(strlen(kb_string)<kb_stringlen)
				{
					kb_string[strlen(kb_string)] = ' ';
					UG_TextboxSetText( &window_1, TXB_ID_0, kb_string );
				}
				break;
			case BUTTON_ID_AT:
				if(strlen(kb_string)<kb_stringlen)
				{
					kb_string[strlen(kb_string)] = '@';
					UG_TextboxSetText( &window_1, TXB_ID_0, kb_string );
				}
				break;
			case BUTTON_ID_PERIOD:
				if(strlen(kb_string)<kb_stringlen)
				{
					kb_string[strlen(kb_string)] = '.';
					UG_TextboxSetText( &window_1, TXB_ID_0, kb_string );
				}
				break;
			case BUTTON_ID_DOTCOM:
				if(strlen(kb_string)<(kb_stringlen-3))
				{
					kb_string[strlen(kb_string)] = '.';
					kb_string[strlen(kb_string)] = 'c';
					kb_string[strlen(kb_string)] = 'o';
					kb_string[strlen(kb_string)] = 'm';
					UG_TextboxSetText( &window_1, TXB_ID_0, kb_string );
				}
				break;
			case BUTTON_ID_KEYBOARD_CHANGE:
				if(kb_ptr == keys_numbers_2 || kb_ptr == keys_numbers)
				{
					keyboard_change(KB_TYPE_QWERTY_LOWER);
				}
				else
				{
					keyboard_change(KB_TYPE_NUMBERS);
				}
				break;
			case BUTTON_ID_ENTER:
				kb_callback();
				break;
			case BUTTON_ID_BACK:
				UG_KeyboardHide();
				break;
			}

			keyboard_scroll();

		}
	}

}

static void keyboard_scroll()
{
	//make sure the string fits, if not cut off beginning
	#define MAX_STR_DISPLAY_LEN 15
	int len = strlen(kb_string);
	if(len > MAX_STR_DISPLAY_LEN)
	{
		UG_TextboxSetText( &window_1, TXB_ID_0, &kb_string[len-MAX_STR_DISPLAY_LEN] );
	}
}

static void keyboard_change(UG_U8 type)
{

	switch(type)
	{
	case KB_TYPE_QWERTY_LOWER:
		kb_ptr = keys_qwerty;
		UG_ButtonSetText( &window_1, BUTTON_ID_KEYBOARD_CHANGE, "?123" );
		UG_ButtonSetText( &window_1, 		BUTTON_ID_SHIFT, "Aa" );
		break;
	case KB_TYPE_QWERTY_UPPER:
		kb_ptr = keys_qwerty_upper;
		UG_ButtonSetText( &window_1, BUTTON_ID_KEYBOARD_CHANGE, "?123" );
		UG_ButtonSetText( &window_1, 		BUTTON_ID_SHIFT, "Aa" );
		break;
	case KB_TYPE_NUMBERS:
		kb_ptr = keys_numbers;
		UG_ButtonSetText( &window_1, BUTTON_ID_KEYBOARD_CHANGE, "ABC" );
		UG_ButtonSetText( &window_1, 		BUTTON_ID_SHIFT, "1/2" );
		break;
	case KB_TYPE_NUMBERS_2:
		kb_ptr = keys_numbers_2;
		UG_ButtonSetText( &window_1, BUTTON_ID_KEYBOARD_CHANGE, "ABC" );
		UG_ButtonSetText( &window_1, 		BUTTON_ID_SHIFT, "2/2" );
		break;
	}

	for(int i=0;i<KB_TOTAL_QWERTY; i++)
	{
		UG_ButtonSetText( &window_1, i, &kb_ptr[i*2] );
	}
}
