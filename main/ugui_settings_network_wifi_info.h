#include "ugui.h"
#include "ugui_config.h"

#ifndef __UGUI_SETTINGS_NETWORK_WIFI_INFO_H
#define __UGUI_SETTINGS_NETWORK_WIFI_INFO_H

UG_RESULT UG_SettingsNetworkWifiInfoCreate();
UG_RESULT UG_SettingsNetworkWifiInfoShow();
UG_RESULT UG_SettingsNetworkWifiInfoHide();

#endif //#ifndef __UGUI_SETTINGS_NETWORK_WIFI_INFO_H
