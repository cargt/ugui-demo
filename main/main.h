/*
 * main.h
 *
 *  Created on: Nov 9, 2017
 *      Author: cargt
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <stdint.h>
#include <stdbool.h>

#include "sdkconfig.h"


#define SW_VERSION_MAJOR        1
#define SW_VERSION_MINOR        10
#define SW_VERSION_MICRO        0

#define HW_VERSION				1

#define get_sw_version() ( (SW_VERSION_MAJOR<<16) | (SW_VERSION_MINOR<<8) | SW_VERSION_MICRO )





#endif /* MAIN_H_ */
