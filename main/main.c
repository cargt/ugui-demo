#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "nvs_flash.h"
#include "task_gui.h"
#include "task_network.h"
#include "task_webserver.h"

#ifdef CONFIG_ENABLE_FW_UPDATE_OTA
#include "task_ota_update.h"
#endif

#include "main.h"

#define TAG "main"

/* Variable holding number of times ESP32 restarted since first boot.
 * It is placed into RTC memory using RTC_DATA_ATTR and
 * maintains its value when ESP32 wakes from deep sleep.
 */
RTC_DATA_ATTR static int bootCount = 0;

esp_err_t event_handler(void *ctx, system_event_t *event)
{
    // Call all the subordinate system event handlers in applicable tasks
    network_event_handler(ctx, event);
    task_webserver_event_handler(ctx,event);
    gui_event_handler(ctx, event);

#ifdef CONFIG_ENABLE_MQTT_CLIENT
    mqtt_client_event_handler(ctx, event);
#endif

#ifdef CONFIG_ENABLE_FW_UPDATE_OTA
    ota_update_event_handler(ctx, event);
#endif

    return ESP_OK;
}

void app_main(void)
{
	 task_gui_init_early();
    ++bootCount;
    ESP_LOGI(TAG, "app_main:boot count =%u", bootCount);
    ESP_LOGI(TAG, "SW VERSION:%u.%u.%u", SW_VERSION_MAJOR, SW_VERSION_MINOR, SW_VERSION_MICRO);
#ifdef CONFIG_ENABLE_FW_UPDATE_OTA
    task_ota_update_printCurrentPartitions();
#endif // CONFIG_ENABLE_FW_UPDATE_OTA

    // Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );

    tcpip_adapter_init();

    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));

#ifdef CONFIG_ENABLE_FW_UPDATE_OTA
    task_ota_update_init();
#endif
    task_network_init();
#ifdef CONFIG_ENABLE_MQTT_CLIENT
    char host[HOST_ADDRESS_LENGTH_STORAGE];
    bool cmdRet = STORAGE_getHostName(host, sizeof host);
    if (cmdRet)
    {
        task_mqtt_client_init(host);
    }
    else
        task_mqtt_client_init(NULL); // use default hostname
#endif

    task_gui_init();
    //wait to bring up webserver
    vTaskDelay(5000 / portTICK_PERIOD_MS);
    task_webserver_init();
}

