#include "main.h"
#include <stdio.h>
#include "ugui_helper.h"
#include "ugui_home.h"
#include "ugui_settings_general.h"

#include "ugui_settings_about.h"




//buttons
#define TXB_ID_HEADER				TXB_ID_0
#define IMG_ID_LOGO					IMG_ID_1
#define IMG_ID_BATTERY				IMG_ID_2
#define IMG_ID_WIFI					IMG_ID_3
#define BUTTON_ID_BACK 				BTN_ID_4
#define TXB_ID_ABOUT				TXB_ID_5
#define TXB_ID_SW_VER				TXB_ID_6
#define MAX_OBJECTS        			7

/* Window 1 */
static UG_WINDOW window_1;
static UG_OBJECT obj_buff_wnd_1[MAX_OBJECTS];
static UG_TEXTBOX tb_header;

static UG_TEXTBOX tb_header;
static UG_IMAGE img_battery;
static UG_IMAGE img_wifi;
static UG_BUTTON button_back;
static UG_TEXTBOX tb_about;
static UG_TEXTBOX tb_sw_ver;

static char sw_ver_string[50];


static void touch_callback( UG_MESSAGE* msg );
static void window_update( ugui_system_event_t evt );

UG_RESULT UG_SettingsAboutCreate()
{
	/*******************************************************
	 * Window 1
	 *******************************************************/
	UG_WindowCreate( &window_1, obj_buff_wnd_1, MAX_OBJECTS, touch_callback );
	UG_WindowSetForeColor( &window_1,SETTINGS_WINDOW_FORECOLOR );
	UG_WindowSetBackColor( &window_1,SETTINGS_WINDOW_BACKCOLOR );
	UG_WindowSetStyle( &window_1, WINDOW_STYLE);

	UG_S16 width = UG_WindowGetInnerWidth(&window_1);
	UG_S16 height = UG_WindowGetInnerHeight(&window_1);

	/*******************************************************
	 * Window Header
	 ******************************************************/

	// text box - window header
	UG_TextboxCreate( &window_1, &tb_header,TXB_ID_HEADER,0,0,width,WINDOW_HEADER_H);
	UG_TextboxSetFont( &window_1, TXB_ID_HEADER, &TXB_HEADER_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_HEADER, TXB_HEADER_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_HEADER, TXB_HEADER_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_HEADER, ALIGN_CENTER );
	UG_TextboxSetText( &window_1, TXB_ID_HEADER, "Cargt uGUI" );

	// image - battery
	UG_ImageCreate( &window_1, &img_battery, IMG_ID_BATTERY,
			HEADER_BATTERY_XS,
			HEADER_BATTERY_YS,
			HEADER_BATTERY_XE,
			HEADER_BATTERY_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_BATTERY, &HEADER_BATTERY_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_BATTERY, 1, TXB_HEADER_BACKCOLOR );

	// image - wifi
	UG_ImageCreate( &window_1, &img_wifi, IMG_ID_WIFI,
			HEADER_WIFI_XS,
			HEADER_WIFI_YS,
			HEADER_WIFI_XE,
			HEADER_WIFI_YE );
	UG_ImageSetBMP( &window_1, IMG_ID_WIFI, &HEADER_WIFI_IMG );
	UG_ImageSetEraseFirstFlag( &window_1, IMG_ID_WIFI, 1, TXB_HEADER_BACKCOLOR );

	//back
	UG_ButtonCreate( &window_1, &button_back, BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_XS,SETTINGS_BUTTON_BACK_YS,SETTINGS_BUTTON_BACK_XE,SETTINGS_BUTTON_BACK_YE);
	UG_ButtonSetFont( &window_1, 		BUTTON_ID_BACK, &SETTINGS_BUTTON_FONT );
	UG_ButtonSetForeColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_FORECOLOR );
	UG_ButtonSetBackColor( &window_1, 	BUTTON_ID_BACK, TXB_HEADER_BACKCOLOR );
	UG_ButtonSetStyle( &window_1, 		BUTTON_ID_BACK, SETTINGS_BUTTON_BACK_STYLE);
	UG_ButtonSetImage( &window_1, BUTTON_ID_BACK, BTN_IMG_CENTER, &SETTINGS_BUTTON_BACK_ICON, SETTINGS_BUTTON_BACK_ICON.width, SETTINGS_BUTTON_BACK_ICON.height );

	/*******************************************************
	 * Window Specific items
	 ******************************************************/
	// text box
	UG_TextboxCreate( &window_1, &tb_about,TXB_ID_ABOUT,SETTINGS_TXB_XS,SETTINGS_TXB_YS,SETTINGS_TXB_XE,SETTINGS_TXB_YE-1);
	UG_TextboxSetFont( &window_1, TXB_ID_ABOUT, &SETTINGS_TXB_FONT );
	UG_TextboxSetForeColor( &window_1, TXB_ID_ABOUT, SETTINGS_TXB_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_ABOUT, SETTINGS_TXB_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_ABOUT, SETTINGS_TXB_ALIGNMENT );
	UG_TextboxSetText( &window_1, TXB_ID_ABOUT, "ABOUT" );

	// sw version
	UG_TextboxCreate( &window_1, &tb_sw_ver, TXB_ID_SW_VER,
			10,
			SETTINGS_TXB_YE+10,
			width-1,
			SETTINGS_TXB_YE+80 );
	UG_TextboxSetFont( &window_1, TXB_ID_SW_VER, &FONT_16 );
	UG_TextboxSetForeColor( &window_1, TXB_ID_SW_VER, SETTINGS_WINDOW_FORECOLOR );
	UG_TextboxSetBackColor( &window_1, TXB_ID_SW_VER, SETTINGS_WINDOW_BACKCOLOR );
	UG_TextboxSetAlignment( &window_1, TXB_ID_SW_VER, ALIGN_CENTER_LEFT );
	sprintf( sw_ver_string, "Sofware version %02d:%02d:%d",SW_VERSION_MAJOR,SW_VERSION_MINOR,SW_VERSION_MICRO);
	UG_TextboxSetText( &window_1, TXB_ID_SW_VER, sw_ver_string );

	return UG_RESULT_OK;
}

UG_RESULT UG_SettingsAboutShow()
{
	return UG_WindowShow( &window_1, window_update );
}

static void window_update( ugui_system_event_t evt )
{
	switch(evt)
	{
	case UGUI_SYSTEM_EVENT_TICK:
		ugui_icon_wifi_update(&window_1,IMG_ID_WIFI);
		ugui_icon_battery_update(&window_1,IMG_ID_BATTERY);
		break;
	case UGUI_SYSTEM_EVENT_WIFI_SCAN_DONE:
		break;
	default:
		break;
	}
}

UG_RESULT UG_SettingsAboutHide()
{
	return UG_WindowHide( &window_1 );
}

static void touch_callback( UG_MESSAGE* msg )
{
	if ( msg->type == MSG_TYPE_OBJECT )
	{
		if ( msg->id == OBJ_TYPE_BUTTON && msg->event == OBJ_EVENT_RELEASED )
		{
			switch(msg->sub_id)
			{
			case BUTTON_ID_BACK:
				UG_SettingsGeneralShow();
				break;
			}
		}
	}

}

