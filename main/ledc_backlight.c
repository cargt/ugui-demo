/*
 * ledc_backlight.c
 *
 *  Created on: Jan 3, 2018
 *      Author: Graham
 */
#include "sdkconfig.h"

#include "driver/ledc.h"
#include "esp_log.h"

#include "ledc_backlight.h"

#define TAG "LEDC_BKL"

#define LEDC_PWM_MAX                    ((1<<13) - 1)
#define LEDC_PWM_RESOLUTION             LEDC_TIMER_13_BIT
#define LEDC_PWM_COMMON_FREQUENCY       5000
#define LEDC_TIMER_MODE                 LEDC_LOW_SPEED_MODE
#define LEDC_TIMER_NUM_BACKLIGHT        LEDC_TIMER_1
#define LEDC_CHANNEL_NUM_BACKLIGHT      LEDC_CHANNEL_4
static ledc_channel_config_t ledc_channel;

//***********************************************
//* Private Prototypes
//***********************************************
static void ledc_backlight_update(ledc_channel_config_t ledc_channel, uint32_t reqDutyCycle);


//***********************************************
//* Public Functions
//***********************************************
void ledc_init(uint8_t gpioNum)
{
    /*
     * Prepare and set configuration of timers
     * that will be used by LED Controller
     */
    ledc_timer_config_t ledc_timer = {
        .bit_num = LEDC_PWM_RESOLUTION,         // resolution of PWM duty
        .freq_hz = LEDC_PWM_COMMON_FREQUENCY,   // frequency of PWM signal
        .speed_mode = LEDC_TIMER_MODE,          // timer mode
        .timer_num = LEDC_TIMER_NUM_BACKLIGHT   // timer index
    };
    // Set configuration of timer1 for low speed channels
    ledc_timer_config(&ledc_timer);

    /*
     * Prepare individual configuration
     * for each channel of LED Controller
     * by selecting:
     * - controller's channel number
     * - output duty cycle, set initially to 0
     * - GPIO number where LED is connected to
     * - speed mode, either high or low
     * - timer servicing selected channel
     *   Note: if different channels use one timer,
     *         then frequency and bit_num of these channels
     *         will be the same
     */

    ledc_channel.channel    = LEDC_CHANNEL_NUM_BACKLIGHT;
    ledc_channel.duty       = LEDC_PWM_MAX;
    ledc_channel.gpio_num   = gpioNum;
    ledc_channel.speed_mode = LEDC_TIMER_MODE;
    ledc_channel.timer_sel  = LEDC_TIMER_NUM_BACKLIGHT;

    // Set LED Controller with previously prepared configuration
    ledc_channel_config(&ledc_channel);

    // Initialize fade service.
    //ledc_fade_func_install(0);

    ESP_LOGI(TAG, "PWM Backlight setup");
} // ledc_init

void ledc_setDutyCycle(uint8_t percent)
{
    uint8_t invPct;
    uint32_t pwmVal;
    if (percent >= 100)
    {
        invPct = 0;
        pwmVal = 0;
    }
    else
    {
        invPct = 100 - percent; // LED driver is through an inverting FET
        // + 50 takes care of integer rounding error (i.e. + 100/2)
        pwmVal = ((((uint32_t) invPct) * LEDC_PWM_MAX) + 50)/ 100;
    }
	ESP_LOGI(TAG, "Backlight PWM changed to effective %u%%; pwm=%u%%-%u", percent, invPct, pwmVal);
    ledc_backlight_update(ledc_channel, pwmVal);
}


//***********************************************
//* Private Functions
//***********************************************
void ledc_backlight_update(ledc_channel_config_t ledc_channel, uint32_t reqPWMValue)
{
    ledc_set_duty(ledc_channel.speed_mode, ledc_channel.channel, reqPWMValue);
    ledc_update_duty(ledc_channel.speed_mode, ledc_channel.channel);
} // ledc_backlight_update
