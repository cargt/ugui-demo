/*
 * ugui_helper.h
 *
 *  Created on: Feb 21, 2018
 *      Author: mark
 */

#ifndef MAIN_UGUI_HELPER_H_
#define MAIN_UGUI_HELPER_H_

#include "ugui.h"

void ugui_icon_wifi_update(UG_WINDOW* window_1, int id);
void ugui_icon_battery_update(UG_WINDOW* window_1, int id);

void ugui_emailKeyboard();
void ugui_emailPasswordKeyboard();
void ugui_verifyEmailCallback(int errorCode, char *data);
void ugui_verifyEmailPasswordCallback(int errorCode, char *data);

#endif /* MAIN_UGUI_HELPER_H_ */
