/*
 * task_network.h
 *
 *  Created on: Nov 15, 2017
 *      Author: cargt
 */

#ifndef MAIN_TASK_NETWORK_H_
#define MAIN_TASK_NETWORK_H_

#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_event.h"

enum {
    // Status bits stay set for as long as the condition is active
    TN_WIFI_CONNECTED_STATUS_BIT        = BIT0,
    TN_WIFI_IP_VALID_STATUS_BIT         = BIT1,

    TN_SYSTEM_SHUTTING_DOWN_STATUS_BIT  = BIT5,

#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
    TN_ETH_CONNECTED_STATUS_BIT         = BIT6,
    TN_ETH_IP_VALID_STATUS_BIT          = BIT7,
#endif

};

/* NOTE: The event bits should not be used by external functions
 * This is static use only.
 * However, they are defined here for visibility.
 * For defensive programming make status and event bits have different values.
 * This will highlight errors quicker if the wrong group is accidentally used.
 * Event groups are up to 24bits
 */
enum {
    // Event bits are set until the event has been acted on
    TN_EVNT_BIT_CONNECTED           = BIT8,
    TN_EVNT_BIT_START_SCAN          = BIT9,
    TN_EVNT_BIT_SCAN_COMPLETE       = BIT10,
    TN_EVNT_BIT_SSID_UPDATE         = BIT11,
    TN_EVNT_BIT_CHECK_SYSTEM_TIME   = BIT12,
	TN_EVNT_BIT_CONNECT   			= BIT13,
};

enum wifi_status_t {
    WIFI_STATUS_NO_INIT,
    WIFI_STATUS_INIT,
    WIFI_STATUS_CONNECTED,
    WIFI_STATUS_DISCONNECTED,
};

#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
enum ethernet_status_t {
    ETH_STATUS_NO_INIT,
    ETH_STATUS_INIT,
    ETH_STATUS_CONNECTED,
    ETH_STATUS_DISCONNECTED,
};
enum ethernet_status_t task_network_getEthernetStatus(void);
#endif

EventGroupHandle_t network_status_group;

/******************************************************************************
 * network_event_handler
 * This event handler is to be called by the central event handler
 * There can only be ONE system event handler
 */
esp_err_t network_event_handler(void *ctx, system_event_t *event);

bool task_network_getIpAddressWifi(char *buf, uint8_t bufSize);
enum wifi_status_t task_network_getWifiStatus(void);

uint8_t task_network_get_rssi();


#ifdef CONFIG_ENABLE_ETHERNET_INTERFACE
bool task_network_getIpAddressEthernet(char *buf, uint8_t bufSize);
enum ethernet_status_t task_network_getEthernetStatus(void);
#endif

int  task_network_init(void);
void task_network_signalSystemShuttingDown(void);
void task_network_sendStartScanEvent(void);

bool task_network_updateSSIDAndPassword(const char *ssid, const char *password);
void task_network_updateMode();

#endif /* MAIN_TASK_NETWORK_H_ */
