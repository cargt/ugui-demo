#include "ugui.h"
#include "ugui_config.h"

#ifndef __UGUI_SETTINGS_NETWORK_WIFI_H
#define __UGUI_SETTINGS_NETWORK_WIFI_H

UG_RESULT UG_SettingsNetworkWifiCreate();
UG_RESULT UG_SettingsNetworkWifiShow();
UG_RESULT UG_SettingsNetworkWifiHide();
void UG_SettingsNetworkWifiSsidUpdate();

#endif //#ifndef __UGUI_SETTINGS_NETWORK_WIFI_H
